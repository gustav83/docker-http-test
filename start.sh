#!/bin/sh

index="/usr/share/nginx/html/index.html"
echo '' > "$index"
echo '<h1 style="position:absolute;top:50%;left:50%;transform:translateX(-50%) translateY(-50%)">' >> "$index"
ifconfig | grep "inet addr" | cut -d ':' -f 2 | cut -d ' ' -f 1 | head -1 >> "$index"
echo '</h1>' >> "$index"

nginx -g "daemon off;"
